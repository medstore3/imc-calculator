<img src="https://cdn4.iconfinder.com/data/icons/logos-3/456/nodejs-new-pantone-black-512.png" align="right" width="50px">

<br><br>

# Calculadora de IMC - Aplicação Nest.js

Esta é uma aplicação Node.js e TypeScript, utilizando o framework Nest.js, que permite calcular o Índice de Massa Corporal (IMC) utilizando o conceito de filas. O IMC é uma medida usada para avaliar se uma pessoa está acima, abaixo ou dentro do peso ideal, com base em sua altura e peso.

## Ambiente de Desenvolvimento

### Pré-requisitos

Certifique-se de ter as seguintes ferramentas instaladas em sua máquina de desenvolvimento:

- Docker
- Docker Compose

### Passo a Passo

1. Clone este repositório em sua máquina local:

```bash
git clone <url_do_repositorio>
```

2. Acesse o diretório raiz do projeto:

```bash
cd <pasta_do_projeto>
```

3. Abra com o Visual Studio Code:

```bash
code .
```

4. Clique no botão: Abrir em um container:

<img src="https://code.visualstudio.com/assets/docs/devcontainers/create-dev-container/dev-container-reopen-prompt.png">

## Funcionamento

A aplicação utiliza uma fila para processar as requisições de cálculo do IMC. Quando uma requisição é recebida, os dados do paciente são adicionados à fila. Em seguida, o cálculo do IMC é realizado para cada paciente na fila.

A fila é processada de forma assíncrona, ou seja, vários cálculos de IMC podem ocorrer simultaneamente. Assim que um cálculo é concluído, o resultado é notificado através de um tópico.

## Contribuição

Contribuições são bem-vindas! Se você quiser adicionar novas rotas ou melhorar as existentes, sinta-se à vontade para enviar um pull request para o repositório da biblioteca no GitLab. Certifique-se de seguir as diretrizes de contribuição e de teste fornecidas no projeto.

## Licença

Esta biblioteca está licenciada sob a **Licença MIT**. Sinta-se à vontade para usá-la em seus projetos comerciais ou pessoais.

<img src="https://cdn4.iconfinder.com/data/icons/logos-3/456/nodejs-new-pantone-black-512.png" width="50px">
