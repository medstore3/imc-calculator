import { Injectable } from '@nestjs/common';
import { SqsMessageHandler } from '@ssut/nestjs-sqs';
import * as AWS from 'aws-sdk';
import { awsConfig } from '../../config/aws.config';
import { CalculatorService } from './calculator.service';
import { MessageProducer } from '../message/message.producer';
import { API_DATA } from '@medstore/common-types';

@Injectable()
export class AppHandler {
  constructor(
    private readonly calculator: CalculatorService,
    private readonly producerService: MessageProducer,
  ) {}

  @SqsMessageHandler(awsConfig.QUEUE_NAME, false)
  async handleMessage(message: AWS.SQS.Message) {
    const dataToCalc = this.hydrateReceivedBody(message.Body);
    const result = await this.calculator.calculate(dataToCalc);
    await this.producerService.sendMessage(
      { name: 'IMC', value: result },
      awsConfig.TOPIC_ARN,
    );
  }

  private hydrateReceivedBody(body: string): API_DATA.DataToCalculate {
    return JSON.parse(body);
  }
}
