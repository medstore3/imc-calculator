export type CalculatorParams = {
  weight: number;
  height: number;
};
