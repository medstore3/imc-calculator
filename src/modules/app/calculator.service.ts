import { Injectable } from '@nestjs/common';
import { CalculatorParams } from './app.types';

@Injectable()
export class CalculatorService {
  async calculate(data: CalculatorParams): Promise<number> {
    return data.weight / (data.height ^ 2);
  }
}
